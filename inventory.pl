#!/usr/bin/perl

use strict;
use Data::Dumper;
use Text::CSV_PP;

my $DEBUG = 1;
local $\ = "\n";

sub die_with_help() {
	die `pod2text $0`;
}

die_with_help() unless @ARGV;

my ($file_a, $file_b) = @ARGV;

# Check existence of input files
unless (-e $file_a) {
	print STDERR "ERROR: File A ($file_a) does not exists";
}

unless (-e $file_b) {
	print STDERR "ERROR: File B ($file_b) does not exists";
}

if (not (-e $file_a and -e $file_b) ) {
	die_with_help();
}

my @data_a;
my %data_b;

# Read file A to hash
open (my $fh,'<',$file_a);
{
	local $/ = "\n"; # Unix line endings
	my $columns = <$fh>;
	chomp($columns);
	my @columns = split(/,/,$columns);

	local $/ = ""; # Need to read entire file into scalar

	my $data = <$fh>;
	open my $text_handle, '<', \$data
		or die "Failed to open the handle: $!";
	my $csv = Text::CSV_PP->new ( { binary => 1 } )
		or die "Cannot use CSV: ".Text::CSV_PP->error_diag ();
		
	while (my $row = $csv->getline($text_handle)) {
		my %hash_row;
		foreach my $j (1..scalar(@columns)) {
			$hash_row{$columns[$j-1]} = $row->[$j-1];
		}
;
		push @data_a,\%hash_row;
	}
}
close $fh;

#print Dumper(\@data_a) if $DEBUG;

# Read file B to hash
# Need to delete first and last lines (column header and column count)
my ($first_line,$last_line);
my $is_first_string = 1;
open (my $fh,'<',$file_b);
{
	local $/ = "\r"; # Mac OS 9 line endings

	while (<$fh>) {
		chomp;
		if ($is_first_string) {
			$is_first_string = 0;
			$first_line = $_;
		}
		next unless $_;
		$data_b{lc($_)}++;
		$last_line = $_;
	}
}
close $fh;

# Delete first and last lines
delete $data_b{lc($first_line)};
delete $data_b{lc($last_line)};

#print Dumper(\%data_b) if $DEBUG;


# For each record, output to CSV file sorted by SKU: Sku, Name, Cost, Count, Actual Count, Difference (Count - Actual Count), Total Cost (supply_price * Actual Count), Total Retail (retail_price * Actual Count)

# sort array ref by SKU

my @sorted_data_a = sort { $a->{sku} cmp $b->{sku} } @data_a ;

#print Dumper(\@sorted_data_a) if $DEBUG;

my %found_sku_in_a;

my %counter_a_sku;

foreach my $record (@sorted_data_a) {
	my ($inventory_Main_Outlet) = ( $record->{inventory_Main_Outlet} =~ /(\d+)\./ );
	
	my $record_name = $record->{name};
	$record_name =~ s/"/""/g;

	$found_sku_in_a{ lc($record->{sku}) } = 1;
	
	$counter_a_sku{ lc($record->{sku}) } ++;
	
	#	"Sku, Name, Cost, Count, Actual Count, Difference (Count - Actual Count), Total Cost (supply_price * Actual Count), Total Retail (retail_price * Actual Count)"

	print
		join(',',
			$record->{sku},
			"$record_name",
			$record->{supply_price},
			$inventory_Main_Outlet,
			$data_b{ lc($record->{sku}) } || 0,
			( $inventory_Main_Outlet - ( $data_b{ lc($record->{sku}) } || 0 ) ),
			( $record->{supply_price} * ( $data_b{ lc($record->{sku}) } || 0 ) ),
			( $record->{retail_price} * ( $data_b{ lc($record->{sku}) } || 0 ) ),
		);

}


# looking for b, not found in a

open (my $fh,'>','errors_'.$file_b);

foreach my $sku (sort keys %data_b) {
	if ( $found_sku_in_a{ $sku } != 1 ) {
		print $fh $sku
	}
}

close $fh;



# looking for duplicates  in a

open (my $fh,'>','errors_'.$file_a);

foreach my $sku (sort keys %counter_a_sku) {
	if ( $counter_a_sku{ $sku } != 1 ) {
		print $fh $sku
	}
}

close $fh;




__END__

=head1 NAME
  inventory

=head1 SYNOPSYS
  ./inventory.pl
       run without parametes will show this help
  ./inventory.pl file_a.csv file_b.csv > file_c.csv
       run with parametes:
        file_a.csv - retail POS system listing all the items
        file_b.csv - record for each item manually scaned in retail store

=head1 DESCRIPTION
  The purpose of the program is to compare actual inventory levels in File B
  to the inventory levels our system thinks we have in File A. The output
  file will be used to identify discrepancies in inventory. In addition, we
  will use the file to record value of inventory on hand.
  
=head1 AUTHOR
  Stas Raskumandrin, stas@raskumandrin.ru, http://stas.raskumandrin.ru

=cut