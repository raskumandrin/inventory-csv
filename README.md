https://www.elance.com/j/write-program-read-two-csv-files-output-one-csv-file/55081761/

I need a program written that will read in two CSV files and output a single CSV file.

File A: An export file from our retail POS system that has SKU data.

File B: CSV file created during our monthly inventory counting process.

Program will read and store certain fields from each record in File A. It will then update the inventory count for each record with in File B with a matching SKU.

Program will then export a CSV file. 

Program will need to run on a Mac or web server. More technical specifications below:


File A is a CSV file downloaded from our retail POS system listing all the items we have set up.
File B is a CSV file with a record for each item we manually scan in our retail store.

File C is a sample of the output desired.

Error Report generated if sku value in a record in File B is not found in File A.

The purpose of the program is to compare actual inventory levels in File B to the inventory levels our system thinks we have in File A. The output file will be used to identify discrepancies in inventory. In addition, we will use the file to record value of inventory on hand.


Basic Logic

1. Read through File A file.
For each record, store type, sku, name, supply_price, retail_price, inventory_Main_Outlet
Each record also needs an actual_count field which is initially set to 0.

2. Read through File B file
Increase value actual_count by 1 for each time sku is found in File B.

If SKU in File B, not found in File A
Update Error Report with sky

3. For each record, output to CSV file sorted by SKU:
Sku, Name, Cost, Count, Actual Count, Difference (Count - Actual Count), Total Cost (supply_price * Actual Count), Total Retail (retail_price * Actual Count)

4. Generate error report, if errors found.

I have one more request. It's come to my attention that the Sku field in File A has some duplicate records. I'd like to add a check for this and if duplicates are found, include in the error-log.