#!/usr/bin/perl

use strict;

foreach (0..($#ARGV)) {
	if (-e $ARGV[$_]) {
		open (my $fh,'<',$ARGV[$_]);
		{
			local $/ = "\n"; # Unix line endings
			<$fh> if $_; # skip all heading except first argiment file
			while (<$fh>) {
				print;
			}
		}
		close($fh);
	}
	else {
		die "Error: file $ARGV[$_] doesn't exist. Not finished."
	}

}

__END__


./join_csv.pl vend-products-Page1.csv vend-products-Page2.csv > file_aa.csv
